class BaseError(Exception):
    default_code = ''

    def __init__(self, message='', code='', details=None):
        self.message = message
        self.code = code or self.default_code
        self.details = details or {}
        super().__init__(message, code, details)

    def __repr__(self):
        return f"[{self.code}] {self.message}"


class ShitHappened(BaseError):
    default_code = 'panic'


class HTTPError(ShitHappened):
    default_status = 500

    def __init__(self, message='', code='', details=None, status=None):
        self.status = status or self.default_status
        super().__init__(message, code, details)
