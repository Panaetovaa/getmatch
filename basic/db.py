import logging
import re
import time

import sqlalchemy as sa
from gino import Gino

from basic.exceptions import ShitHappened


logger = logging.getLogger(__name__)


class Database(Gino):
    async def attach(self, dsn):
        self._log_dsn(dsn)
        await self.set_bind(dsn, logging_name='sql')

    async def detach(self):
        bind = self.pop_bind()
        if bind:
            await bind.close()

    def _log_dsn(self, dsn):
        dsn = str(dsn)
        dsn_without_credentials = self._get_dsn_without_credentials(dsn)
        if dsn_without_credentials:
            logger.info(f"Connection to db: {dsn_without_credentials}")
        else:
            logger.info("Cannot log database DSN. It will contain credentials.")

    def _get_dsn_without_credentials(self, dsn):
        matched = re.match('[^@]+@(.*(:.*)?\/.*)', dsn)
        if matched:
            return matched.group(1)
        return ""


class Databases:
    def __init__(self, databases_settings):
        self._settings = databases_settings

        self._databases = {}
        for name in self._settings:
            self._databases[name] = Database()

    async def attach(self):
        for name, dsn in self._settings.items():
            await self._databases[name].attach(dsn)

    async def detach(self):
        for db in self._databases.values():
            await db.detach()

    def __getattr__(self, name):
        db = self._databases.get(name)
        if not db:
            raise ShitHappened(f"Unknown database {name}")

        return db


def create_table_proxy(db):
    class Proxy(db.Model):
        id = db.Column(db.Integer, autoincrement=True, primary_key=True)
        created_at = db.Column(db.Float, default=time.time)
        updated_at = db.Column(db.Float, default=time.time, onupdate=time.time)

        @classmethod
        async def count(cls, query):
            query = query.with_only_columns([sa.func.count()])

            # FIXME
            # workaround for error
            # AttributeError: 'NoneType' object has no attribute 'scalar'
            query = query.where(cls.id > 0)

            return await query.gino.scalar()

    return Proxy
