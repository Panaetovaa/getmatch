import json
import logging

import sanic

from sanic.views import HTTPMethodView

from basic.sanic_tools.validation import create_validation_decorator


logger = logging.getLogger(__name__)


class RequestProxy:
    def __init__(self, request):
        self._request = request

    def __getattr__(self, name):
        return getattr(self._request, name)


class BaseView(HTTPMethodView):
    request_model = None
    request_model_post = None
    request_model_put = None

    query_model = None
    query_model_get = None
    query_model_post = None
    query_model_put = None
    query_model_delete = None

    response_model = None
    response_model_get = None
    response_model_post = None
    response_model_put = None
    response_model_delete = None

    class json_encoder(json.JSONEncoder):
        def default(self, o):
            if isinstance(o, tuple):
                return str(o)

            try:
                return o.json()
            except KeyError:
                return json.JSONEncoder.default(self, o)

    def get_handler(self, request):
        method = request.method.lower()
        handler = getattr(self, method)

        request_model = getattr(self, f'request_model_{method}', None) or self.request_model
        query_model = getattr(self, f'query_model_{method}', None) or self.query_model

        handler = create_validation_decorator(
            body_model=request_model,
            query_model=query_model,
        )(handler)
        return handler

    async def dispatch_request(self, request, *args, **kwargs):
        request = RequestProxy(request)
        handler = self.get_handler(request)
        response = await handler(request, *args, **kwargs)
        if isinstance(response, dict):
            method = request.method.lower()
            response_model = getattr(self, f'response_model_{method}', None) or self.response_model
            if response_model:
                response_payload = response_model(**response).dict()
            else:
                response_payload = response

            return sanic.response.json(
                response_payload, dumps=json.dumps, cls=self.json_encoder
            )

        return response
