from typing import Dict

import sanic


def validation_errors(errors: Dict):
    return sanic.response.json({
        "type": "validation-error",
        "details": errors,
    }, status=422)
