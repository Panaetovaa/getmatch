from functools import wraps

from pydantic import ValidationError

from basic.exceptions import ShitHappened
from basic.sanic_tools.exceptions import RequestValidationError


BODY_METHODS = ["POST", "PUT", "PATCH"]


def create_validation_decorator(query_model=None, body_model=None, path_model=None):
    def decorator(f):
        @wraps(f)
        async def decorated_function(request, *args, **kwargs):
            try:
                result = validate_request(request, query_model, body_model, path_model)
            except ValidationError as e:
                raise RequestValidationError(details=e.errors()) from e
            except Exception as e:
                raise RequestValidationError(message=str(e)) from e

            request.payload = result['payload']
            request.query_args = result['query_args']
            request.path_args = result['path_args']
            return await f(request, *args, **kwargs)

        return decorated_function

    return decorator


def validate_request(request, query_model, body_model, path_model):
    payload = None
    query_args = None
    path_args = None

    if body_model and request.method not in BODY_METHODS:
        raise ShitHappened(
            f"Http method '{request.method}' does not contain a payload,"
            "yet a `Pyndatic` model for body was supplied"
        )

    if body_model:
        payload = body_model(**(request.json or {}))

    if query_model:
        args = request.args
        args = {k: v[0] for k, v in args.items()}
        query_args = query_model(**args)

    if path_model:
        path_args = path_model(**request.match_info)

    return dict(
        payload=payload,
        query_args=query_args,
        path_args=path_args,
    )
