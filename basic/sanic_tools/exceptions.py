from typing import Dict

from basic.exceptions import HTTPError


class RequestValidationError(HTTPError):
    default_code = 'validation-error'
    default_status = 400

    @classmethod
    def create(cls, field__message: Dict):
        errors = []
        for f, m in field__message.items():
            errors.append({'loc': [f], 'msg': m})

        return cls(details=errors)
