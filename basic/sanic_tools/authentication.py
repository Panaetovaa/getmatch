import base64
import logging

from pydantic import BaseModel


logger = logging.getLogger(__name__)


class Credentials(BaseModel):
    username: str
    password: str


def get_basic_credentials(request) -> Credentials:
    """ Parse basic authentication credentials.
    Authorization: Basic dGVzdDE6dGVzdDI=
    """
    username, password = "", ""

    header = request.headers.get("Authorization", '').strip()
    if header:
        method, encoded_credentials = _split_basic_authentication_header(header)
        if method.upper() != 'BASIC':
            logger.warning(f"Received not basic authentication header: {method}")
        else:
            username, password = _decode_credentials(encoded_credentials)

    return Credentials(username=username, password=password)


def _split_basic_authentication_header(header):
    chunks = header.split()
    if len(chunks) == 2:
        return chunks
    else:
        return "", ""


def _decode_credentials(encoded_credentials):
    try:
        username, password = base64.b64decode(encoded_credentials).decode().split(":")
    except Exception as e:
        logger.warning(f"Cannot decode credentials: {e}")
        return "", ""
    else:
        return username, password
