import sentry_sdk

from sentry_sdk.integrations.logging import LoggingIntegration


def setup(settings):
    if not getattr(settings, 'SENTRY_DSN', ''):
        return

    sentry_sdk.init(
        dsn=str(settings.SENTRY_DSN),
        integrations=[LoggingIntegration()]
    )
