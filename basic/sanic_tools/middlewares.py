import logging

import sanic.response
from sanic.exceptions import SanicException
from sanic.handlers import ErrorHandler as BaseErrorHandler

from basic.exceptions import HTTPError


logger = logging.getLogger(__name__)


class ErrorHandler(BaseErrorHandler):
    def default(self, request, exception):
        logger.exception(exception)
        if isinstance(exception, SanicException):
            return super().default(request, exception)

        else:
            return self.process_exception(request, exception)

    def process_exception(self, request, exception):
        if isinstance(exception, HTTPError):
            return self.process_http_error(request, exception)

        else:
            return self.process_unknown_error(request, exception)

    def process_http_error(self, request, exception):
        return sanic.response.json({"error": {
            "type": exception.code,
            "message": exception.message or "",
            "details": exception.details or {},
        }}, status=exception.status)

    def process_unknown_error(self, request, exception):
        return sanic.response.json({"error": {
            "type": "panic",
            "message": "Something wrong happened."
        }}, status=500)
