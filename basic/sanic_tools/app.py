import sanic

from basic.sanic_tools import sentry
from basic.sanic_tools import middlewares
from basic.db import Databases


def create_app(name, *, settings, error_handler=None):
    app = sanic.Sanic(name=name)

    sentry.setup(settings)
    app.error_handler = error_handler or middlewares.ErrorHandler()
    app.databases = Databases(getattr(settings, 'DATABASES', {}))

    @app.listener('before_server_start')
    async def _(app, loop):  # NOQA
        await app.databases.attach()

    @app.listener('before_server_stop')
    async def _(app, loop):
        await app.databases.detach()

    @app.route('/ping')
    async def _(request):
        return sanic.response.text('pong')

    return app
