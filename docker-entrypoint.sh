#!/usr/bin/env bash

case "$1" in
    "help")
        echo "Please use of next parameters to start: "
        echo "  > webserver: Start webserver"
        echo "  > bash: Start bash shell"
        ;;
    "bash")
        echo "Starting bash ..."
        exec bash
        ;;

    "migrate")
        echo "Database migrate"
        alembic upgrade head
        ;;

    "tests")
        echo "Starting tests ..."
        exec python -m pytest -v
        ;;

    "webserver")
        echo "Starting webserver ..."
        exec python getmatch/main.py
        ;;
    *)
        echo "Unknown command '$1'. please use one of: [webserver, bash, help]"
        exit 1
        ;;
esac
