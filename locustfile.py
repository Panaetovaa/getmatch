import hashlib
import json
import random

from locust import HttpUser, task


SHORT_CODES = []


class Consumer(HttpUser):
    @task
    def generate(self):
        response = self.client.post("/urls", json={
            'url': f'http://{hashlib.sha256().hexdigest()}.com',
        })
        short_url = json.loads(response.text)['short_url']
        short_code = short_url.split("/")[-1]
        SHORT_CODES.append(short_code)

    @task(5)
    def redirect(self):
        short_code = random.choice(SHORT_CODES)
        self.client.get(f"/urls/{short_code}")

    @task
    def stats(self):
        short_code = random.choice(SHORT_CODES)
        self.client.get(f"/urls/{short_code}/stats")
