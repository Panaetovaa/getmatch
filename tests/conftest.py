import asyncio

from contextlib import suppress

import pytest

from getmatch import routes  # NOQA


@pytest.fixture(autouse=True)
async def teardown():
    yield

    pending = asyncio.all_tasks()
    for task in pending:
        task.cancel()
        with suppress(asyncio.CancelledError):
            await task
