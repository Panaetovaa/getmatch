import json
import time

import pytest

from getmatch.app import app


@pytest.mark.api
def test_success_generation_of_short_code():
    data = {'url': 'http://google.com'}
    _, response = app.test_client.post('/urls', data=json.dumps(data))

    assert response.status == 200
    response_data = json.loads(response.body)
    assert response_data.get('short_url')


@pytest.mark.api
def test_fail_generation_of_short_code_if_no_url():
    data = {}
    _, response = app.test_client.post('/urls', data=json.dumps(data))

    assert response.status == 400
    response_data = json.loads(response.body)
    assert response_data.get('error')
    assert response_data['error']['type'] == 'validation-error'
    assert response_data['error']['details'][0]['loc'] == ['url']
    assert response_data['error']['details'][0]['type'] == 'value_error.missing'


@pytest.mark.api
def test_fail_generation_of_short_code_if_wrong_name():
    data = {'url2': 'asda'}
    _, response = app.test_client.post('/urls', data=json.dumps(data))

    assert response.status == 400
    response_data = json.loads(response.body)
    assert response_data.get('error')
    assert response_data['error']['type'] == 'validation-error'
    assert response_data['error']['details'][0]['loc'] == ['url']
    assert response_data['error']['details'][0]['type'] == 'value_error.missing'


@pytest.mark.api
def test_fail_generation_of_short_code_if_too_short():
    data = {'url': 'a'}
    _, response = app.test_client.post('/urls', data=json.dumps(data))

    assert response.status == 400
    response_data = json.loads(response.body)
    assert response_data.get('error')
    assert response_data['error']['type'] == 'validation-error'
    assert response_data['error']['details'][0]['loc'] == ['url']
    assert response_data['error']['details'][0]['type'] == 'value_error.any_str.min_length'


@pytest.mark.api
def test_redirect():
    google_url = 'http://www.google.com'
    data = {'url': google_url}
    _, response = app.test_client.post('/urls', data=json.dumps(data))

    assert response.status == 200
    response_data = json.loads(response.body)
    short_url = response_data.get('short_url')

    short_code = short_url.split("/")[-1]
    _, response = app.test_client.get(f'/urls/{short_code}')

    assert response.status == 200
    assert response.url == google_url


@pytest.mark.api
def test_redirect_not_found():
    data = {'url': 'http://google.com'}
    _, response = app.test_client.post('/urls', data=json.dumps(data))

    assert response.status == 200
    response_data = json.loads(response.body)
    short_url = response_data.get('short_url')

    short_code = short_url.split("/")[-1]
    _, response = app.test_client.get(f'/urls/{short_code}xxx')

    assert response.status == 406


@pytest.mark.api
def test_updating_short_code():
    data = {'url': 'http://google.com'}
    _, response = app.test_client.post('/urls', data=json.dumps(data))

    assert response.status == 200
    response_data = json.loads(response.body)
    short_url = response_data.get('short_url')

    short_code = short_url.split("/")[-1]
    getmatch_url = "https://getmatch.io"
    app.test_client.put(f'/urls/{short_code}', data=json.dumps({
        "url": getmatch_url,
    }))

    _, response = app.test_client.get(f'/urls/{short_code}')
    assert response.url == getmatch_url
    assert response.status == 200


@pytest.mark.api
def test_stats():
    data = {'url': 'http://google.com'}
    _, response = app.test_client.post('/urls', data=json.dumps(data))

    assert response.status == 200
    response_data = json.loads(response.body)
    short_url = response_data.get('short_url')

    short_code = short_url.split("/")[-1]

    app.test_client.post('/inner/stats_window', data=json.dumps({
        "value": 3,
    }))

    app.test_client.get(f'/urls/{short_code}')
    _, response = app.test_client.get(f'/urls/{short_code}/stats')
    count = json.loads(response.body)['count']
    assert count == 1

    app.test_client.get(f'/urls/{short_code}')
    _, response = app.test_client.get(f'/urls/{short_code}/stats')
    count = json.loads(response.body)['count']
    assert count == 2

    time.sleep(3)
    _, response = app.test_client.get(f'/urls/{short_code}/stats')
    count = json.loads(response.body)['count']
    assert count == 0
