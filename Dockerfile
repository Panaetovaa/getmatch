FROM python:3.9-slim-buster as base

ARG NEXUS_PYPI_INDEX_URL

ENV HOME="/dude"
ENV PYTHONPATH=$HOME:$PYTHONPATH

RUN groupadd -g 999 dudes && \
    useradd -u 999 -m -g dudes -s /bin/bash dude && \
    mkdir -p ${HOME} && chown dude:dudes $HOME

WORKDIR ${HOME}

COPY --chown=dude:dudes ./getmatch ${HOME}/getmatch
COPY --chown=dude:dudes ./basic ${HOME}/basic

COPY --chown=dude:dudes requirements.txt ${HOME}
COPY --chown=dude:dudes docker-entrypoint.sh ${HOME}

RUN apt update && apt install -y build-essential && \
    pip install -i "${NEXUS_PYPI_INDEX_URL:-https://pypi.org/simple}" -r "$HOME/requirements.txt"

USER dude
ENTRYPOINT ["/dude/docker-entrypoint.sh"]

FROM base AS service
CMD ["webserver"]

FROM base AS tests
COPY --chown=dude:dudes tests ${HOME}/tests
RUN pip install -i "${NEXUS_PYPI_INDEX_URL:-https://pypi.org/simple}" -r "$HOME/tests/requirements.txt"
CMD ["tests"]
