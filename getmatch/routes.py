import logging
import time

import asyncpg
import sanic
from pydantic import BaseModel
from pydantic import Field

from basic.sanic_tools.views import BaseView
from basic.exceptions import HTTPError

from getmatch.app import app
from getmatch.models import ShortCodes
from getmatch.models import Hits


logger = logging.getLogger(__name__)


STATS_WINDOW = 3600 * 24 * 24
URL_FIELD = Field(..., min_length=3)


class StatsView(BaseView):
    class response_model(BaseModel):
        count: int

    async def get(self, request, short_code):
        qs = Hits.query\
            .where(Hits.code == short_code)\
            .where(Hits.created_at >= time.time() - STATS_WINDOW)

        count = await Hits.count(qs)
        return {
            "count": count,
        }


app.add_route(StatsView.as_view(), '/urls/<short_code>/stats')


class _SetStatsWindowView(BaseView):
    class request_model(BaseModel):
        value: int

    async def post(self, request):
        global STATS_WINDOW
        STATS_WINDOW = request.payload.value
        return {}


app.add_route(_SetStatsWindowView.as_view(), '/inner/stats_window')


class ShortCodesView(BaseView):
    async def get(self, request, short_code):
        record = await ShortCodes.query.where(ShortCodes.code == short_code).gino.first()
        if not record:
            raise HTTPError("Cannot find a long url", 'not-found', status=406)

        await Hits.create(code=short_code)
        return sanic.response.redirect(record.url)

    class request_model_put(BaseModel):
        url: str = URL_FIELD

    async def put(self, request, short_code):
        new_url = request.payload.url

        record = await ShortCodes.query.where(ShortCodes.code == short_code).gino.first()
        if not record:
            raise HTTPError("Cannot find record by code", 'not-found', status=406)

        await record.update(url=new_url).apply()
        return {}

    async def delete(self, request, short_code):
        await ShortCodes.delete.where(ShortCodes.code == short_code).gino.status()
        return {}


app.add_route(ShortCodesView.as_view(), '/urls/<short_code>')


class GenerateCodeView(BaseView):
    class request_model(BaseModel):
        url: str = URL_FIELD

    class response_model(BaseModel):
        short_url: str

    async def post(self, request):
        url = request.payload.url

        record = None
        for _ in range(5):
            try:
                record = await ShortCodes.create(
                    url=url,
                    code=ShortCodes.generate_new_code(),
                )
            except asyncpg.exceptions.UniqueViolationError:
                pass

            else:
                break

        if not record:
            raise HTTPError("Cannot create a new short code", 'collision', status=409)

        return {
            'short_url': f"{request.host}/urls/{record.code}",
        }


app.add_route(GenerateCodeView.as_view(), '/urls')
