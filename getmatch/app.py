from basic.sanic_tools.app import create_app

from getmatch import settings


app = create_app('Getmatch', settings=settings)
