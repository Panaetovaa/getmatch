import logging
import uuid

from getmatch import settings
from getmatch.db import db
from getmatch.db import Table

logger = logging.getLogger(__name__)


class ShortCodes(Table):
    __tablename__ = 'getmatch_short_codes'

    code = db.Column(db.String(32), nullable=False, unique=True)
    url = db.Column(db.String(2000), nullable=False)

    @classmethod
    def generate_new_code(cls):
        return uuid.uuid4().hex[:settings.CODE_LENGTH]


class Hits(Table):
    __tablename__ = 'getmatch_hits'

    code = db.Column(db.String(32), nullable=False)
