from basic.db import create_table_proxy

from getmatch.app import app


db = app.databases.default
Table = create_table_proxy(db)
