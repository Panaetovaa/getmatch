import logging
import logging.config

from getmatch import settings
from getmatch import routes  # NOQA
from getmatch.app import app


logger = logging.getLogger(__name__)


def main():
    logging.config.dictConfig(settings.LOGGING)
    app.run(host=settings.HOST, port=settings.PORT, debug=settings.DEBUG)


if __name__ == "__main__":
    main()
