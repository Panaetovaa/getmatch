import os
from basic.exceptions import ShitHappened


HOST = os.getenv("GETMATCH_HOST", "0.0.0.0")
PORT = os.getenv("GETMATCH_HOST", 8000)
DEBUG = os.getenv("GETMATCH_DEBUG", 'false') in ['false', '0', 'f']

DATABASES = {
    'default': os.getenv("GETMATCH_DB_DSN", "postgresql://postgres:postgres@db:5432/getmatch"),
}

try:
    CODE_LENGTH = int(os.getenv("GETMATCH_CODE_LENGTH", 5))
except ValueError as e:
    raise ShitHappened("Code length is not a number ") from e


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'default': {
            'format': "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt': "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'console': {
            'level': os.getenv('GETMATCH_LOGGERS_HANDLERS_LEVEL', 'INFO'),
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': os.getenv('GETMATCH_LOGGERS_HANDLERS_LEVEL', 'INFO'),
        },
        'gino': {
            'level': os.getenv('GETMATCH_LOGGERS_GINO_LEVEL', 'INFO'),
        }
    }
}

if os.getenv("GETMATCH_GRAYLOG_HOST", ""):
    LOGGING['handlers']['graylog'] = {
        "level": os.getenv("GETMATCH_LOG_LEVEL_GRAYLOG", "INFO"),
        "class": "pygelf.handlers.GelfUdpHandler",
        "formatter": "default",
        "host": os.getenv("GETMATCH_GRAYLOG_HOST", "graylog"),
        "port": int(os.getenv("GETMATCH_GRAYLOG_PORT", "12201")),
        "compress": bool(os.getenv("GETMATCH_GRAYLOG_COMPRESS", "True")),
        "_environment": os.getenv("GETMATCH_GRAYLOG_ENVIRONMENT", "SECENV"),
        "_app_name": "storage",
        "include_extra_fields": True,
        "_container_id": os.getenv("HOSTNAME"),
        "_service": "none",
    }
    LOGGING['loggers']['']['handlers'].append('graylog')

SENTRY_DSN = os.getenv('GETMATCH_LOGGING_SENTRY_DSN')
